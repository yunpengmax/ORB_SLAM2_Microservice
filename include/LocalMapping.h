/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LOCALMAPPING_H
#define LOCALMAPPING_H

#include "KeyFrame.h"
#include "Map.h"
#include "LoopClosing.h"
#include "Tracking.h"
#include "KeyFrameDatabase.h"
#include "MongoIO.h"

#include <ros/ros.h>

#include <mutex>


namespace ORB_SLAM2
{

class Tracking;
class LoopClosing;
class Map;

class LocalMapping
{
public:
    LocalMapping(Map* pMap,  const float bMonocular, ros::NodeHandle* _nh= nullptr);


    void SetLoopCloser(LoopClosing* pLoopCloser);

    void SetTracker(Tracking* pTracker);

    // Main function
    void Run();

    // TODO 每次Tracking产生一个新的关键帧都会调用改函数，因此该函数可以作为数据库写入的入口
    // 该函数需要在Tracking中重写，在Mapping中修改为消息的回调函数
    void InsertKeyFrame(KeyFrame* pKF);
    // 暂时改为如下函数
    void InsertKeyFrame(const MongoIO::ID &id);
    //! Buyi
    void InsertKeyFrame(const std::string _str);

    // Thread Synch
    void RequestStop();
    void RequestReset();
    bool Stop();
    void Release();
    bool isStopped();
    bool stopRequested();
    bool AcceptKeyFrames();
    void SetAcceptKeyFrames(bool flag);
    bool SetNotStop(bool flag);

    void InterruptBA();

    void RequestFinish();
    bool isFinished();

    int KeyframesInQueue()
    {
        unique_lock<std::mutex> lock(mMutexNewKFs);
        return mlNewKeyFrames.size();
    }

    void Update_NewtoDatabase();
    void Update_KFtoMap(std::pair<std::string, std::set<std::string> > &tKFBuffer);
    void Update_MPtoMap(std::pair<string, std::set<std::string> > &tMPBuffer);

protected:

    bool CheckNewKeyFrames();
    void ProcessNewKeyFrame();
    void CreateNewMapPoints();

    void MapPointCulling();
    void SearchInNeighbors();

    void KeyFrameCulling();

    cv::Mat ComputeF12(KeyFrame* &pKF1, KeyFrame* &pKF2);

    cv::Mat SkewSymmetricMatrix(const cv::Mat &v);

    bool mbMonocular;

    void ResetIfRequested();
    bool mbResetRequested;
    std::mutex mMutexReset;

    bool CheckFinish();
    void SetFinish();
    bool mbFinishRequested;
    bool mbFinished;
    std::mutex mMutexFinish;

    Map* mpMap;

    LoopClosing* mpLoopCloser;
    Tracking* mpTracker;

    // TODO 接口修改，由ROS topic发送给Mapping线程MongoIO::ID
    std::list<KeyFrame*> mlNewKeyFrames;
    std::list<MongoIO::ID> mlNewKeyFramesID;

    KeyFrame* mpCurrentKeyFrame;

    std::list<MapPoint*> mlpRecentAddedMapPoints;

    std::mutex mMutexNewKFs;

    //TODO 把标志改为rospara　http://wiki.ros.org/roscpp/Overview/Parameter%20Server
    bool mbAbortBA;

    bool mbStopped;
    bool mbStopRequested;
    bool mbNotStop;
    std::mutex mMutexStop;

    bool mbAcceptKeyFrames;
    std::mutex mMutexAccept;

    //Buyi
    ros::Publisher mLMtoLC_advertiser;
    ros::Publisher mLMUpdate_advertiser;

    // !rain
    ros::Publisher mLMUpdateKFDb;
    ros::NodeHandle* mnh;
    // < <frame/point, id, var> >
    //std::vector< std::vector<std::std::string>> mvVartoPublish;
    //std::vector<std::pair<bool, std::pair<std::string, string> > > mvVartoPublish;

};

} //namespace ORB_SLAM

#endif // LOCALMAPPING_H
