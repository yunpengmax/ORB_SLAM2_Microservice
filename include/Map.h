/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAP_H
#define MAP_H

#include "KeyFrameDatabase.h"
#include "MapPoint.h"
#include "KeyFrame.h"
#include <set>
#include <mutex>

#include "MongoIO.h"

namespace ORB_SLAM2
{

class MapPoint;
class KeyFrame;
class KeyFrameDatabase;

class Map
{
public:

    Map(KeyFrameDatabase* pDB, const std::string uri, bool drop = false);

    void AddKeyFrame(KeyFrame* pKF);
    void AddMapPoint(MapPoint* pMP);
    //! database
    KeyFrame* AddKeyFrame(const MongoIO::ID &id);
    MapPoint* AddMapPoint(bsoncxx::document::view &view, const MongoIO::ID &id);
    void EraseMapPoint(MapPoint* pMP);
    void EraseKeyFrame(KeyFrame* pKF);
    void SetReferenceMapPoints(const std::vector<MapPoint*> &vpMPs);
    void InformNewBigChange();
    int GetLastBigChangeIdx();

    std::vector<KeyFrame*> GetAllKeyFrames();
    std::vector<MapPoint*> GetAllMapPoints();
    std::vector<MapPoint*> GetReferenceMapPoints();

    long unsigned int MapPointsInMap();
    long unsigned  KeyFramesInMap();

    long unsigned int GetMaxKFid();

    void clear();

    vector<KeyFrame*> mvpKeyFrameOrigins;

    std::mutex mMutexMapUpdate;

    // This avoid that two points are created simultaneously in separate threads (id conflict)
    std::mutex mMutexPointCreation;

    //! for load database keyframe
    static std::string sDatabase;
    static std::string sCollectionKF;
    static std::string sCollectionMP;
    MongoIO::IO::Ptr mpMongoIO;

    KeyFrame* GetDatabaseKeyFrame(const bsoncxx::types::b_oid &oid);
    void SetDatabaseKeyFrame(KeyFrame* pKF);
    MapPoint* GetDatabaseMapPoint(const bsoncxx::types::b_oid &oid);
    void SetDatabaseMapPoint(MapPoint* pMP);

    void AddKeyFrameUpdateBuffer(KeyFrame* pKF, const std::string para);
    void AddMapPointUpdateBuffer(MapPoint* pMP, const std::string para);
    std::map<KeyFrame*, std::set<std::string> > GetKeyFrameUpdateBuffer();
    std::map<MapPoint*, std::set<std::string> > GetMapPointUpdateBuffer();

    // TODO 临时变量
    KeyFrameDatabase* mpKeyFrameDB;


protected:

    std::set<MapPoint*> mspMapPoints;
    std::set<KeyFrame*> mspKeyFrames;

    std::vector<MapPoint*> mvpReferenceMapPoints;

    long unsigned int mnMaxKFid;

    // Index related to a big change in the map (loop closure, global BA)
    int mnBigChangeIdx;

    std::map<std::string, KeyFrame*> mmDataBaseKeyFrames;
    std::map<std::string, MapPoint*> mmDataBaseMapPoints;
    std::map<KeyFrame*, std::set<std::string> > mmKeyFrameSaveBuffer;
    std::map<MapPoint*, std::set<std::string> > mmMapPointSaveBuffer;
    bool mbKFBuffClear;
    bool mbMPBuffClear;

    std::mutex mMutexMap;
    std::mutex mMutexDatabase;
    std::mutex mMutexKeyFrameBuffer;
    std::mutex mMutexMapPointBuffer;

};

} //namespace ORB_SLAM

#endif // MAP_H
